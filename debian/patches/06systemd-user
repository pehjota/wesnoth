From fc4d11b2cb3851590e2175d05ea4e27df914dc14 Mon Sep 17 00:00:00 2001
From: "P. J. McDermott" <pj@pehjota.net>
Date: Thu, 28 Dec 2023 13:37:31 -0500
Subject: [PATCH] systemd: Run as _wesnoth:_wesnoth

systemd 246+ logs a warning message, because running as "nobody" is
unsafe:
https://github.com/systemd/systemd/blob/v246/NEWS#L106-L113

Prefix names with underscore, as recommended by systemd documentation:
https://www.freedesktop.org/software/systemd/man/latest/sysusers.d.html

Forwarded: https://github.com/wesnoth/wesnoth/pull/8169
Origin: https://github.com/wesnoth/wesnoth/commit/fc4d11b2cb3
Applied-Upstream: 1.16.12
---
 SConstruct                                  | 3 ++-
 changelog_entries/systemd_nobody.md         | 2 ++
 packaging/systemd/SConscript                | 3 ++-
 packaging/systemd/wesnothd.service.in       | 4 ++--
 packaging/systemd/wesnothd.service.scons.in | 2 ++
 packaging/systemd/wesnothd.sysusers.conf.in | 2 ++
 packaging/systemd/wesnothd.tmpfiles.conf.in | 2 +-
 7 files changed, 13 insertions(+), 5 deletions(-)
 create mode 100644 changelog_entries/systemd_nobody.md
 create mode 100644 packaging/systemd/wesnothd.sysusers.conf.in

diff --git a/SConstruct b/SConstruct
index 99a2f10d975..fa3d6837694 100755
--- a/SConstruct
+++ b/SConstruct
@@ -786,7 +786,8 @@ if not access(fifodir, F_OK):
     env.Alias("install-wesnothd", fifodir)
 if env["systemd"]:
     env.InstallData("prefix", "wesnothd", "#packaging/systemd/wesnothd.service", "lib/systemd/system")
-    env.InstallData("prefix", "wesnothd", "#packaging/systemd/wesnothd.conf", "lib/tmpfiles.d")
+    env.InstallData("prefix", "wesnothd", "#packaging/systemd/wesnothd/tmpfiles.conf", "lib/tmpfiles.d")
+    env.InstallData("prefix", "wesnothd", "#packaging/systemd/wesnothd/sysusers.conf", "lib/sysusers.d")
 
 # Wesnoth campaign server
 env.InstallBinary(campaignd)
diff --git a/changelog_entries/systemd_nobody.md b/changelog_entries/systemd_nobody.md
new file mode 100644
index 00000000000..7429fbe901c
--- /dev/null
+++ b/changelog_entries/systemd_nobody.md
@@ -0,0 +1,2 @@
+ ### Security Fixes
+   * Run wesnothd server as `_wesnoth:_wesnoth` instead of `nobody:users`, improving safety and fixing a warning message in systemd 246+
diff --git a/packaging/systemd/SConscript b/packaging/systemd/SConscript
index 47fa3550bfb..feea984e18e 100644
--- a/packaging/systemd/SConscript
+++ b/packaging/systemd/SConscript
@@ -1,4 +1,5 @@
 Import("env")
 
 env.ScanReplace("wesnothd.service", "wesnothd.service.scons.in")
-env.ScanReplace("wesnothd.conf", "wesnothd.tmpfiles.conf.in")
+env.ScanReplace("wesnothd.tmpfiles.conf", "wesnothd.tmpfiles.conf.in")
+env.ScanReplace("wesnothd.sysusers.conf", "wesnothd.sysusers.conf.in")
diff --git a/packaging/systemd/wesnothd.service.in b/packaging/systemd/wesnothd.service.in
index a78b0f66478..76447f6b6d2 100644
--- a/packaging/systemd/wesnothd.service.in
+++ b/packaging/systemd/wesnothd.service.in
@@ -23,8 +23,8 @@ ExecStopPost=/bin/rm -f @FIFO_DIR@/socket
 
 SyslogIdentifier=Wesnothd@BINARY_SUFFIX@
 WorkingDirectory=@FIFO_DIR@
-User=nobody
-Group=users
+User=_wesnoth
+Group=_wesnoth
 
 # Additional security-related features
 # (when using the -c option, do not use ProtectHome)
diff --git a/packaging/systemd/wesnothd.service.scons.in b/packaging/systemd/wesnothd.service.scons.in
index 14490676d28..11127e710fc 100644
--- a/packaging/systemd/wesnothd.service.scons.in
+++ b/packaging/systemd/wesnothd.service.scons.in
@@ -4,6 +4,8 @@ After=network.target
 
 [Service]
 ExecStart=%bindir/wesnothd
+User=_wesnoth
+Group=_wesnoth
 
 [Install]
 WantedBy=multi-user.target
diff --git a/packaging/systemd/wesnothd.sysusers.conf.in b/packaging/systemd/wesnothd.sysusers.conf.in
new file mode 100644
index 00000000000..220fdc187bd
--- /dev/null
+++ b/packaging/systemd/wesnothd.sysusers.conf.in
@@ -0,0 +1,2 @@
+u _wesnoth -
+g _wesnoth -
diff --git a/packaging/systemd/wesnothd.tmpfiles.conf.in b/packaging/systemd/wesnothd.tmpfiles.conf.in
index a446d5a9ba0..adad9c4bf73 100644
--- a/packaging/systemd/wesnothd.tmpfiles.conf.in
+++ b/packaging/systemd/wesnothd.tmpfiles.conf.in
@@ -1 +1 @@
-d @FIFO_DIR@ 0700 nobody users -
+d @FIFO_DIR@ 0700 _wesnoth _wesnoth -
