Source: wesnoth-1.16
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Build-Depends:
 cmake (>= 3.7),
 debhelper-compat (= 13),
 libboost-coroutine-dev       (>= 1.66.0),
 libboost-filesystem-dev      (>= 1.66.0),
 libboost-iostreams-dev       (>= 1.66.0),
 libboost-locale-dev          (>= 1.66.0),
 libboost-program-options-dev (>= 1.66.0),
 libboost-random-dev          (>= 1.66.0),
 libboost-regex-dev           (>= 1.66.0),
 libboost-system-dev          (>= 1.66.0),
 libboost-test-dev            (>= 1.66.0),
 libboost-thread-dev          (>= 1.66.0),
 libcairo2-dev (>= 1.10.0),
 libdbus-1-dev,
 libfontconfig-dev (>= 2.4.1),
 libpango1.0-dev (>= 1.22.0),
 libreadline-dev,
 libsdl2-dev (>= 2.0.8),
 libsdl2-image-dev (>= 2.0.2),
 libsdl2-mixer-dev (>= 2.0.0),
 libssl-dev (>= 1.0),
 libvorbis-dev,
 pkgconf,
Standards-Version: 4.6.2
Uploaders:
 Rhonda D'Vine <rhonda@debian.org>,
 Vincent Cheng <vcheng@debian.org>,
 P. J. McDermott <pj@pehjota.net>,
Homepage: https://www.wesnoth.org/
Vcs-Git: https://salsa.debian.org/games-team/wesnoth.git
Vcs-Browser: https://salsa.debian.org/games-team/wesnoth
Rules-Requires-Root: no

Package: wesnoth-1.16-data
Architecture: all
Multi-Arch: foreign
Depends:
 fonts-adf-oldania,
 fonts-dejavu-core,
 fonts-dejavu-extra,
 fonts-droid-fallback,
 fonts-lato,
 ${misc:Depends},
Suggests:
 wesnoth-1.16-music,
Description: data files for Wesnoth (branch 1.16)
 This package contains the sound files and graphics for Wesnoth. It is required
 for being able to play wesnoth or create maps with the editor.
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

# TODO: Remove package in wesnoth-1.18
Package: wesnoth-1.16-core
Architecture: all
Section: oldlibs
Depends:
 wesnoth-1.16 (<< ${binary:Version}.~),
 wesnoth-1.16 (>= ${binary:Version}),
 wesnoth-1.16-data (= ${source:Version}),
 ${misc:Depends},
Description: fantasy turn-based strategy game (1.16, transitional package)
 This is a transitional dummy metapackage.  It can safely be removed.
 The main program for Wesnoth is now provided by the wesnoth-1.16 package.
 To play multiplayer games or user-made add-ons without installing all the
 official campaigns and music, install the wesnoth-1.16 package without
 recommended packages.
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

# TODO: Remove package after Debian trixie
Package: wesnoth-core
Architecture: all
Section: oldlibs
Depends:
 wesnoth-1.16 (>= ${binary:Version}),
 wesnoth-1.16-data (= ${source:Version}),
 ${misc:Depends},
Description: fantasy turn-based strategy game (transitional package)
 This is a transitional dummy metapackage.  It can safely be removed.
 To play the latest stable version of Wesnoth, install the wesnoth package.
 To play multiplayer games or user-made add-ons without installing all the
 official campaigns and music, install the wesnoth package without recommended
 packages.
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16
Architecture: any
Depends:
 wesnoth-1.16-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 wesnoth-1.16-did (= ${source:Version}),
 wesnoth-1.16-dm (= ${source:Version}),
 wesnoth-1.16-dw (= ${source:Version}),
 wesnoth-1.16-ei (= ${source:Version}),
 wesnoth-1.16-httt (= ${source:Version}),
 wesnoth-1.16-l (= ${source:Version}),
 wesnoth-1.16-low (= ${source:Version}),
 wesnoth-1.16-music (= ${source:Version}),
 wesnoth-1.16-nr (= ${source:Version}),
 wesnoth-1.16-sof (= ${source:Version}),
 wesnoth-1.16-sota (= ${source:Version}),
 wesnoth-1.16-sotbe (= ${source:Version}),
 wesnoth-1.16-thot (= ${source:Version}),
 wesnoth-1.16-trow (= ${source:Version}),
 wesnoth-1.16-tsg (= ${source:Version}),
 wesnoth-1.16-ttb (= ${source:Version}),
 wesnoth-1.16-utbs (= ${source:Version}),
Breaks:
 wesnoth-1.16-core (<< 1:1.16.11-1.~),
Replaces:
 wesnoth-1.16-core (<< 1:1.16.11-1.~),
# TODO: Remove Provides in wesnoth-1.18
Provides:
 wesnoth-1.16-core,
Description: fantasy turn-based strategy game (branch 1.16)
 This package contains the main program for Wesnoth.  If you only want to
 play a few official campaigns, multiplayer games over the network with other
 players, user-made add-ons, and/or without music, install this package without
 recommended packages.
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth
Architecture: all
Depends:
 wesnoth-1.16 (>= ${binary:Version}),
 wesnoth-1.16-data (= ${source:Version}),
 ${misc:Depends},
Description: fantasy turn-based strategy game (metapackage)
 This metapackage pulls in the main Wesnoth package which allows you to play
 using the latest stable version.  If you only want to play a few official
 campaigns, multiplayer games over the network with other players, user-made
 add-ons, and/or without music, install this package without recommended
 packages.
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-music
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: music files for Wesnoth (branch 1.16)
 This package contains the music files for Wesnoth. It is not required but
 gives nice background music and encouraged.
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-music
Architecture: all
Multi-Arch: foreign
Depends:
 wesnoth-1.16-data (= ${source:Version}),
 wesnoth-1.16-music (= ${source:Version}),
 ${misc:Depends},
Description: music files for Wesnoth (metapackage)
 This metapackage pulls in the music package for use with the latest stable
 version of wesnoth.
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-server
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 adduser,
 ${misc:Depends},
 ${shlibs:Depends},
Description: multiplayer network server for Wesnoth (branch 1.16)
 This package contains the multiplayer network server for Wesnoth.  You need it
 if you want to host multiplayer games on your computer and don't want to use
 the official servers.
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-tools
Architecture: all
Multi-Arch: foreign
Depends:
 python3:any,
 wesnoth-1.16-data (= ${source:Version}),
 ${misc:Depends},
 ${perl:Depends},
Recommends:
 imagemagick | graphicsmagick-imagemagick-compat,
Enhances:
 wesnoth-1.16 (>> 1:1.16.11-1.~),
 wesnoth-1.16-core (<< 1:1.16.11-1.~),
Description: tools for campaign developers for Wesnoth (branch 1.16)
 This package contains various tools for Wesnoth that are especially useful for
 campaign developers, including but not limited to scripts supporting the
 generation and checking of WML (Wesnoth Markup Language).  You can find them
 in the directory /usr/share/games/wesnoth/1.16/data/tools
 after installation of the package.
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-httt
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "Heir to the Throne" official campaign for Wesnoth (branch 1.16)
 This package contains the "Heir to the Throne" campaign for Wesnoth:
 "Fight to regain the throne of Wesnoth, of which you are the legitimate
 heir."
 (Novice level, 23 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-tsg
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "The South Guard" official campaign for Wesnoth (branch 1.16)
 This package contains the "The South Guard" campaign for Wesnoth:
 "A young Knight, Deoran, is dispatched to take command of the South Guard...
 Note: This campaign is designed as an introduction to Wesnoth. The 'Civilian'
 difficulty level is aimed at first-time players."
 (Novice level, 9 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-trow
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "The Rise of Wesnoth" official campaign for Wesnoth (branch 1.16)
 This package contains the "The Rise of Wesnoth" campaign for Wesnoth:
 "Lead Prince Haldric through the destruction of the Green Isle and across the
 Ocean to establish the very kingdom of Wesnoth itself. The confrontation with
 Lich-Lord Jevyan awaits..."
 (Intermediate level, 24 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-ttb
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "A Tale of Two Brothers" official campaign for Wesnoth (branch 1.16)
 This package contains the "A Tale of Two Brothers" campaign for Wesnoth:
 "An evil mage is threatening the small village of Maghre and its inhabitants.
 The village’s mage sends to his warrior brother for help, but not all goes as
 planned. Can you help?"
 (Novice level, 4 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-ei
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "The Eastern Invasion" official campaign for Wesnoth (branch 1.16)
 This package contains the "The Eastern Invasion" campaign for Wesnoth:
 "There are rumors of undead attacks on the eastern border of Wesnoth. You, an
 officer in the Royal Army, have been sent to the eastern front to protect the
 villagers and find out what is happening."
 (Intermediate level, 16 scenarios.)"
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-utbs
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "Under the Burning Suns" official campaign for Wesnoth (branch 1.16)
 This package contains the "Under the Burning Suns" campaign for Wesnoth:
 "In the distant future a small band of elves struggles to survive amidst the
 ruins of fallen empires. Lead your people out of the desert on an epic journey
 to find a new home."
 (Expert level, 10 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-did
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "Descent Into Darkness" official campaign for Wesnoth (branch 1.16)
 This package contains the "Descent Into Darkness" campaign for Wesnoth:
 "Learn the dark arts of necromancy in order to save your people from an orcish
 incursion."
 (Intermediate level, 12 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-nr
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "Northern Rebirth" official campaign for Wesnoth (branch 1.16)
 This package contains the "Northern Rebirth" campaign for Wesnoth:
 "For the people of Dwarven Doors the choice was stark: either drudge as
 downtrodden slaves for the orcs until the end of their brief and miserable
 lives, or risk all for freedom and rise up against their cruel overlords.
 Little did they suspect that their struggle would be the hinge of great events
 that might restore the Northlands to the glory they had once known."
 (Expert level, 13 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-sof
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "The Sceptre of Fire" official campaign for Wesnoth (branch 1.16)
 This package contains the "The Sceptre of Fire" campaign for Wesnoth:
  "The land of Wesnoth's banner bold
  Comes not from its own land;
  It comes from Dwarfdom, grim and old
  Made by a runesmith's hand.
  So now I tell from whence it came -
  The Fire-sceptre great -
  And of the makers of the same,
  Their tale I now relate..."
 (Expert level, 9 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-sotbe
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "Son of the Black-Eye" official campaign for Wesnoth (branch 1.16)
 This package contains the "Son of the Black-Eye" campaign for Wesnoth:
 "Your father Karun Black-Eye was the greatest orcish leader that ever lived.
 Now, as his son, it's up to you to thwart the selfish designs of the humans
 who have broken the old agreements with the orcs and are bent upon taking your
 lands. Unite the warring orcish tribes, bring together the Orcish Council and
 call up the Great Horde to send the human-worms and their wose-born allies to
 the land of the dead!"
 (Expert level, 18 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-l
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "Liberty" official campaign for Wesnoth (branch 1.16)
 This package contains the "Liberty" campaign for Wesnoth:
 "As the shadow of civil war lengthens across Wesnoth, a band of hardy
 marchlanders revolts against the tyranny of Queen Asheviere. To win their way
 to freedom, they must defeat not just the trained blades of Wesnothian troops
 but darker foes including orcs and undead."
 (Intermediate level, 8 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-thot
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "The Hammer of Thursagan" official campaign for Wesnoth (branch 1.16)
 This package contains the "The Hammer of Thursagan" campaign for Wesnoth:
 "In the first years of the Northern Alliance, an expedition from Knalga seeks
 out their kin at Kal Kartha and to learn the fate of the legendary Hammer of
 Thursagan. The perils of their journey through the wild Northern Lands, though
 great, pale beside the evil they will face at its end."
 (Intermediate level, 11 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-low
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "Legend of Wesmere" official campaign for Wesnoth (branch 1.16)
 This package contains the "Legend of Wesmere" campaign for Wesnoth:
 "The tale of Kalenz, the High Lord who rallied his people after the second
 orcish invasion of the Great Continent and became the most renowned hero in
 the recorded history of the Elves."
 (Intermediate level, 18 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-dm
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "Delfador's Memoirs" official campaign for Wesnoth (branch 1.16)
 This package contains the "Delfador's Memoirs" campaign for Wesnoth:
 "Wesnoth seems to be slipping inexorably into chaos, as marauding orcs pour
 south across the Great River, and mysterious and deadly creatures roam the
 night. Who is the shadowy Iliah-Malal? Can you defeat him before he destroys
 all life in Wesnoth?"
 (Intermediate level, 19 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-dw
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "Dead Water" official campaign for Wesnoth (branch 1.16)
 This package contains the "Dead Water" campaign for Wesnoth:
 "You are Kai Krellis, son and heir of the last merman king but only a child. A
 necromancer is turning your subjects into undead slaves! Lead your people on a
 mission to convince a powerful mer-sorceress to help you repel the invasion.
 The oceans near the Northern Lands are perilous, so you will need cunning and
 bravery to survive. But first you need to gain the respect of your troops!"
 (Intermediate level, 10 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.

Package: wesnoth-1.16-sota
Depends:
 wesnoth-1.16 (<< ${wesnoth:Max-Version}),
 wesnoth-1.16 (>= ${wesnoth:Min-Version}),
 ${misc:Depends},
Architecture: all
Description: "Secrets of the Ancients" official campaign for Wesnoth (branch 1.16)
 This package contains the "Secrets of the Ancients" campaign for Wesnoth:
 "Rediscover the secrets known by the lich lords of the Green Isle. They knew
 how to live forever, so why can't you?"
 (Intermediate level, 21 scenarios.)
 .
 Battle for control of villages, using variety of units which have advantages
 and disadvantages in different types of terrains and against different types
 of attacks.  Units gain experience and advance levels, and are carried over
 from one scenario to the next in a campaign.
